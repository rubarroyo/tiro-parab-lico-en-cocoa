//
//  TiroParabolico.m
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 18/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import "TiroParabolico.h"
#import <math.h>

@implementation TiroParabolico
@synthesize g, V0, alfa, t, distancia_max, altura_max, tiempo_total;

-(id) init
{
    g=9.8;
    V0=100;
    alfa=45*M_PI/180;
    return self;
}

-(id) initWithValues:(double)velocidad alfa:(double)angulo
{
    g=9.8;
    V0=velocidad;
    alfa=angulo*M_PI/180;
    return self;
}

// Implementacion de Propiedades

-(void)setV0:(double)newValue
{
    if(newValue>=0)
        V0=newValue;
}

-(void)setAlfa:(double)newValue
{
    // sin redondeo newValue=round(newValue*100)/100;
    alfa=newValue * M_PI / 180;
}

-(double)alfa
{
    return alfa * 180 / M_PI;
}

-(double)distancia_max
{
    return round(distancia_max*100)/100;
    // Puede que este mal. Solo quiero redondear a dos unidades decimales.
    // Hacer lo mismo para la propiedad t "tiempo" y "altura" altura_max
}

-(double)tiempo_total
{
    return round(tiempo_total*100)/100;
}

-(double)altura_max
{
    return round(altura_max*100)/100;
}

// Fin de Propiedades

-(NSMutableArray *)CalcularPuntosReales:(int)numpuntos xrealmax:(double)xrealmax xrealmin:(double)xrealmin
{
    NSMutableArray * puntos_reales;
    puntos_reales=[[NSMutableArray alloc] init];
    
    NSPoint punto;
    float tiempo;
    
    for(int i=0; i<numpuntos; i++){
        punto.x=xrealmin + i * (xrealmax - xrealmin) / numpuntos;
        tiempo = punto.x / (V0 * cos(alfa));
        punto.y=V0*sin(alfa)*tiempo-(g*pow(tiempo,2) / 2);
        
        // ...........
    }
    
    return puntos_reales;
}

-(void)CalcularDatosFisicos
{
    distancia_max=pow(V0, 2) * sin(2 * alfa) / g;
    altura_max=pow(V0, 2) * pow(sin(alfa), 2) / (2 * g);
    tiempo_total=2 * V0 * sin(alfa) / g;
}

-(double)calcular_xrealmax
{
    return pow(V0, 2) * sin(2 * 45 * M_PI / 180) / g + 500;
    // Calculo la distancia maxima a la que puede llegar el experimento utilizando el angulo de 45.
    // Sumo 500 para desplazar aún más la base de las X.
}

-(double)valueAt:(float)x bounds:(NSRect)bounds
{
    float tiempo;
    float y;
    
    double xrealmax = [self calcular_xrealmax];
    double yrealmax = xrealmax; // La escala del EJEX debe ser igual al EJEY
    
    x=(xrealmax/(bounds.size.width-25-25)) * x;
    tiempo=x/(V0 * cos(alfa));
    y=V0*sin(alfa)*tiempo-(g*pow(tiempo,2)/2);
    y=(bounds.size.width/yrealmax) * y;
    
    return y;
}

@end
