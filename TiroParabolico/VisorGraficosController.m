//
//  VisorGraficosController.m
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 19/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import "VisorGraficosController.h"

@interface VisorGraficosController ()

@end

@implementation VisorGraficosController

- (void)windowDidLoad {
    [super windowDidLoad];
    NSLog(@"Fichero NIB cargado");
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    
    nc = [NSNotificationCenter defaultCenter];
    
    // Registrar como observadores
    [nc addObserver:self selector:@selector(handleAumentarContador:) name:@"aumentarContadorNotification" object:nil];

}

-(id) init
{
    if (![super initWithWindowNibName:@"VisorGraficosWindow"])
        return nil;
    return self;
}

-(id)initWithWindow:(NSWindow *)window
{
    self=[super initWithWindow:window];
    if(self){
        // initialization code here.
    }
    return self;
}

-(IBAction)colorSeleccionado:(id)sender
{
    NSColor * colorFondo;
    colorFondo = [selectorColor color];
    
    // Enviar evento para cambiar el color:
    NSString * notificationName = @"backgroundColorNotification";
    
    NSDictionary * datosColor;
    NSArray * valores;
    NSArray * keys = [[NSArray alloc] initWithObjects:@"red", @"green", @"blue", @"alpha", nil];
    
    NSNumber * red = [NSNumber numberWithFloat:[colorFondo redComponent]];
    NSNumber * green = [NSNumber numberWithFloat:[colorFondo greenComponent]];
    NSNumber * blue = [NSNumber numberWithFloat:[colorFondo blueComponent]];
    NSNumber * alpha = [NSNumber numberWithFloat:[sliderOpacidad floatValue]];
    
    valores = [[NSArray alloc] initWithObjects:red, green, blue, alpha, nil];
    
    datosColor = [[NSDictionary alloc] initWithObjects:valores
                                                     forKeys:keys];
    
    [nc postNotificationName:notificationName object:self userInfo:datosColor];
}

-(IBAction)fondoImagenButton:(id)sender
{
    NSString * notificationName = @"fondoImagenNotification";
    [nc postNotificationName:notificationName object:self];
    NSLog(@"Enviado evento de cambio de fondo\n");
}

-(IBAction)iniciarAnimacion:(id)sender
{
// ... lanzar evento y...
    NSString * notificationName = @"iniciarAnimacionNotification";
    NSLog(@"Enviando notificacion de comienzo de animacion");
    [nc postNotificationName:notificationName object:self];
    
    if([buttonIniciarAnimacion state]!=NSOffState){
        [buttonIniciarAnimacion setState:NSOnState];
        [buttonIniciarAnimacion setTitle:@"Pausar"];
    }
    else
    {
        [buttonIniciarAnimacion setState:NSOffState];
        [buttonIniciarAnimacion setTitle:@"Iniciar"];
    }
    
}

-(IBAction)detenerAnimacion:(id)sender
{
    NSString * notificationName = @"iniciarAnimacionNotification";
    NSLog(@"Enviando notificacion de DETENCIÓN de animacion");
    NSDictionary * informacion;
    informacion = [[NSDictionary alloc] initWithObjectsAndKeys:@"1",@"detener", nil];
    [nc postNotificationName:notificationName object:self userInfo:informacion];
    
    if([buttonIniciarAnimacion state]!=NSOffState){
        [buttonIniciarAnimacion setState:NSOffState];
        [buttonIniciarAnimacion setTitle:@"Iniciar"];
    }
}

-(void)handleAumentarContador:(NSNotification *)aNotification
{
    NSInteger contadorActual = [contadorImpactos integerValue];
    contadorActual++;
    [contadorImpactos setIntegerValue:contadorActual];
}

-(IBAction)sonidoCheckBox:(id)sender
{
    NSString * notificationName = @"sonidoOnOffNotification";
    NSLog(@"Enviando notificacion de Sonido ON/OFF");
    [nc postNotificationName:notificationName object:self];
}

@end
