//
//  ExperimentView.h
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 19/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class TiroParabolico;

@interface ExperimentView : NSView
{  
    NSBezierPath * myPath;
    NSBezierPath * ejeX;
    NSBezierPath * ejeY;
    
    NSPoint puntoReferencia;
    
    TiroParabolico * experimento;
    
    NSColor *colorFondo;
    NSImage * imagenFondo;
    NSImage * imagenAnimado;
    NSImage * imagenObjetivo;
    
    NSTimer * temporizador;
    NSTimer * temporizadorNuevaPosicion;
    NSTimer * temporizadorDesplazamiento;
    NSTimer * temporizadorBotar;
    
    NSRect rectanguloAnimado;
    NSRect rectanguloObjetivo;
    NSRect rectDestino;
    
    NSAffineTransform * tf;
    
    int id_fondo;
    
    int salto; // en desarrollo
    
    bool impacto;
    
    double coorX;
    
    NSSound * soundDisparo;
    NSSound * soundImpacto;
    
}

-(void)handleLoadExperiment:(NSNotification *)aNotification;
-(void)handleColorChanged:(NSNotification *)aNotification;
-(void)handleImagenBackground:(NSNotification *)aNotification;
-(void)handleIniciarAnimacion:(NSNotification *)aNotification;
-(void)handleSonidoOnOff:(NSNotification *)aNotification;

-(void)siguienteMovimiento:(NSTimer *) aTimer;
-(void)calcularNuevaPosicion:(NSTimer *) aTimer;
-(void)moverNuevaPosicion:(NSTimer *) aTimer;
-(void)botarObjetivo:(NSTimer *) aTimer;

-(void)realizarImpacto;

-(void)dibujarEjes;
-(void)dibujarExperimento;

@end
