//
//  MainController.h
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 18/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class TablaExperimentosWindowController;
@class VisorGraficosController;

@interface MainController : NSWindowController <NSWindowDelegate>
{
    IBOutlet NSButton * tablaExperimentosButton;
    IBOutlet NSButton * visorGraficoButton;
    
    TablaExperimentosWindowController * experimentosController;
    VisorGraficosController * visorController;
    
}

-(IBAction)tablaExperimentosButton:(id)sender;
-(IBAction)visorGraficoButton:(id)sender;

@end