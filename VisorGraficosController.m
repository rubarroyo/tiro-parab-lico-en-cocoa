//
//  VisorGraficosController.m
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 19/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import "VisorGraficosController.h"

@interface VisorGraficosController ()

@end

@implementation VisorGraficosController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

@end
