//
//  TablaExperimentosWindowController.h
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 14/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class TiroParabolico;

@interface TablaExperimentosWindowController : NSWindowController <NSTableViewDataSource, NSTableViewDelegate, NSControlTextEditingDelegate>
{
    IBOutlet NSTextField * speedTextField;
    IBOutlet NSTextField * angleTextField;
    IBOutlet NSSlider * angleSlider;
    IBOutlet NSTableView * aTableView;
    IBOutlet NSButton * buttonDelete;
    IBOutlet NSButton * loadExperiment;
    IBOutlet NSTextField * totalDistanceTextField;
    IBOutlet NSTextField * maximumHeightTextField;
    IBOutlet NSTextField * totalTimeTextField;
    IBOutlet NSButton * buttonSaveExperiment;
    
    TiroParabolico * tParabolico;
    
    NSMutableArray * experimentosArray;
    
}

-(IBAction)angleSlider:(id)sender;
-(IBAction)deleteExperiment:(id)sender;
-(IBAction)loadExperiment:(id)sender;
-(IBAction)saveExperiment:(id)sender;
-(void)actualizarTextFields;
-(void)enviarEvento:(TiroParabolico*)experimento;

@end
