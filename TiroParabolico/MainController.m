//
//  MainController.m
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 18/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import "MainController.h"
#import "TablaExperimentosWindowController.h"
#import "VisorGraficosController.h"

@interface MainController ()

@end

@implementation MainController

-(id) init
{
    self=[super init];    
    return self;
}

-(void)awakeFromNib
{
    //[buttonAdd setEnabled:NO];
    //[buttonDelete setEnabled:NO];
}

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

-(BOOL)windowShouldClose:(id)sender
{
    NSInteger respuesta;
    respuesta=NSRunAlertPanel(@"Atención", @"¿Está seguro de que desea cerrar la ventana?", @"No", @"Sí", nil);
    if(respuesta==NSAlertDefaultReturn)
        return NO;
    else
    {
        [NSApp terminate:self];
        return YES;
    }
}

-(IBAction)tablaExperimentosButton:(id)sender
{
    if(!experimentosController){
        experimentosController=[[TablaExperimentosWindowController alloc] init];
    }
    NSLog(@"panel %@\n",experimentosController);
    [experimentosController showWindow:self];
}

-(IBAction)visorGraficoButton:(id)sender
{
    if(!visorController){
        visorController=[[VisorGraficosController alloc] init];
    }
    NSLog(@"panel %@\n",visorController);
    [visorController showWindow:self];
}

-(IBAction)showExperimentosPanel:(id)sender
{
    if(!experimentosController){
        experimentosController=[[TablaExperimentosWindowController alloc] init];
    }
    NSLog(@"panel %@\n",experimentosController);
    [experimentosController showWindow:self];
}
@end
