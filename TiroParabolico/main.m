//
//  main.m
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 14/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
