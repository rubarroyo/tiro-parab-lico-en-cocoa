//
//  ExperimentView.m
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 19/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import "ExperimentView.h"
#import "TiroParabolico.h"

@implementation ExperimentView

-(id) initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        // Registrar como observadores
        NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(handleLoadExperiment:) name:@"LoadExperimentToShow" object:nil];
        [nc addObserver:self selector:@selector(handleColorChanged:) name:@"backgroundColorNotification" object:nil];
        [nc addObserver:self selector:@selector(handleImagenBackground:) name:@"fondoImagenNotification" object:nil];
        [nc addObserver:self selector:@selector(handleIniciarAnimacion:) name:@"iniciarAnimacionNotification" object:nil];
        [nc addObserver:self selector:@selector(handleSonidoOnOff:) name:@"sonidoOnOffNotification" object:nil];
        
        // initilizacion code here.
        NSRect r = [self bounds];
        puntoReferencia.x=r.origin.x + 25;
        puntoReferencia.y=r.origin.y + 25;
        experimento = [[TiroParabolico alloc] init];
        
        colorFondo=[NSColor colorWithRed:0 green:0 blue:0 alpha:0]; // Color por defecto transparente
        imagenFondo = [NSImage imageNamed:@"angry-birds-fondo.jpg"]; // Imagen cargada al inicializar
        
        imagenAnimado = [NSImage imageNamed:@"angry.png"]; // Imagen para cargar en el rectangulo animado
        imagenObjetivo = [NSImage imageNamed:@"green-pig.png"];
        
        // Rectangulo animado
        rectanguloAnimado.origin.x=puntoReferencia.x-25;
        rectanguloAnimado.origin.y=puntoReferencia.y-25;
        
        rectanguloAnimado.size.width=50;
        rectanguloAnimado.size.height=50;
        
        // Rectangulo objetivo
        rectanguloObjetivo.origin.x=[self bounds].size.width-200; /* las posiciones sucesivas se calculan
                                                                   automaticamente por temporizador */
        rectanguloObjetivo.origin.y=puntoReferencia.y;
        
        rectanguloObjetivo.size.width=90;
        rectanguloObjetivo.size.height=90;
        
        // Temporizador Calcular Nueva Posicion del Objetivo
        if (temporizadorNuevaPosicion == nil) {
            temporizadorNuevaPosicion = [NSTimer scheduledTimerWithTimeInterval:1
                                                                         target:self
                                                                       selector:@selector(calcularNuevaPosicion:)
                                                                       userInfo:nil
                                                                        repeats:YES];
        }
        
        if (temporizadorBotar == nil)
        {
            temporizadorBotar = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                                 target:self
                                                               selector:@selector(botarObjetivo:)
                                                               userInfo:nil
                                                                repeats:YES];
        }
        
        // Sonidos
        soundDisparo = [NSSound soundNamed:@"angry_birds_sonido_lanzamiento.wav"];
        soundImpacto = [NSSound soundNamed:@"angry_birds_lanzamiento_del_big_brother.wav"];
        
    }
    return self;
    
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    NSRect bounds = [self bounds];
    
    // Imagen de fondo y Color de filtro
        [imagenFondo drawInRect:bounds fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
        NSLog(@"Aplicando el color: %@\n", colorFondo);
        [colorFondo set];
        [NSBezierPath fillRect:bounds]; // Descomentar para quitar image y poner fondo de color.
    
    // Ejes X e Y
    [self dibujarEjes];
    [[NSColor blackColor] setStroke];
    [ejeX stroke];
    [ejeY stroke];
    
    // Para dibujar la grafica
    [self dibujarExperimento];
    [[NSColor whiteColor] setStroke];
    //[[NSColor redColor] setStroke];
    [myPath stroke];
    
    // Cuadro de siguiente movimeinto. Para depurar.
    [[NSColor redColor] set];
    //[NSBezierPath fillRect:rectDestino];
    
    
    // Dibujar Rectangulo Objetivo
    [[NSColor blueColor] set];
    //[NSBezierPath fillRect:rectanguloObjetivo];
    [imagenObjetivo drawInRect:rectanguloObjetivo fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
    
    // Dibujar Rectangulo Animado
    //[NSBezierPath fillRect:rectanguloAnimado];
    [imagenAnimado drawInRect:rectanguloAnimado fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
    
    }

-(void)handleLoadExperiment:(NSNotification *)aNotification
{
    NSLog(@"NOTIFICACION DE CARGA DE EXPERIMENTO RECIBIDA\n");
    
    // Obteniendo datos de la notificacion
    NSDictionary * notificationInfo = [aNotification userInfo];
    
    NSNumber * velocidad = [notificationInfo objectForKey:@"velocidad"];
    NSNumber * angulo = [notificationInfo objectForKey:@"angulo"];
    
    // Creando experimento con los datos recibidos
    experimento = [[TiroParabolico alloc] initWithValues:[velocidad doubleValue] alfa:[angulo doubleValue]];
    
    [self dibujarExperimento];
    NSLog(@"Redibujando experimento con Velocidad: %@ - Angulo %@\n",velocidad, angulo);
}

-(void)dibujarEjes
{
    NSRect bounds = [self bounds];
    
    NSPoint ejeX1, ejeX2;
    NSPoint ejeY1, ejeY2;
    
    ejeX1.x=puntoReferencia.x;
    ejeX1.y=puntoReferencia.y;
    ejeX2.x=bounds.size.width - 25;
    ejeX2.y=ejeX1.y;
    
    ejeY1.x=puntoReferencia.x;
    ejeY1.y=puntoReferencia.y;
    ejeY2.x=ejeY1.x;
    ejeY2.y=bounds.size.height - 25;
    
    // EJES X E Y
    ejeX=[[NSBezierPath alloc] init];
    [ejeX setLineWidth:2];
    [ejeX moveToPoint:ejeX1];
    [ejeX lineToPoint:ejeX2];
    
    ejeY=[[NSBezierPath alloc] init];
    [ejeY setLineWidth:2];
    [ejeY moveToPoint:ejeY1];
    [ejeY lineToPoint:ejeY2];
    
}

-(void)dibujarExperimento
{
    NSRect bounds = [self bounds];
    
    // Proceso de dibujado de gráfica
    
    if(myPath!=nil)
        [myPath removeAllPoints];
    
    myPath=[[NSBezierPath alloc] init];
    [myPath setLineWidth:2];
    [myPath moveToPoint:puntoReferencia];
    
    for(int i = puntoReferencia.x; i<bounds.size.width - 25; i++)
    {
        [myPath lineToPoint: NSMakePoint(i, [experimento valueAt:i-25 bounds:bounds]+25)];
    }
    
    
    [self setNeedsDisplay:YES];
}

-(void)handleColorChanged:(NSNotification *)aNotification
{
    NSLog(@"NOTIFICACION DE CAMBIO DE COLOR RECIBIDO\n");
    
    // Obteniendo datos de la notificacion
    NSDictionary * notificationInfo = [aNotification userInfo];
    
    float red = [[notificationInfo objectForKey:@"red"] floatValue];
    float green = [[notificationInfo objectForKey:@"green"] floatValue];
    float blue = [[notificationInfo objectForKey:@"blue"] floatValue];
    float alpha = [[notificationInfo objectForKey:@"alpha"] floatValue];
    
    NSLog(@"VALOR DE ALPHA=%f", alpha);
    
    // Creando color con los datos recibidos
    colorFondo = [NSColor colorWithRed:red green:green blue:blue alpha:alpha];
    [self setNeedsDisplay:YES];
    
}

-(void)handleImagenBackground:(NSNotification *)aNotification
{
    // DESARROLLAR FUNCIONALIDAD; SINO QUITAR: YA ESTA SUPLIDO EN OTRO LUGAR.
    switch (id_fondo) {
        case 1:
            imagenFondo = [NSImage imageNamed:@"fondo_angry_2.jpg"];
            id_fondo++;
            break;
            
        case 2:
            imagenFondo = [NSImage imageNamed:@"fondo-angry-rio.png"];
            id_fondo++;
            break;
            
        case 3:
            imagenFondo = [NSImage imageNamed:@"angry-birds-fondo.jpg"];
            id_fondo++;
            break;
            
        default:
            imagenFondo = [NSImage imageNamed:@"angry_fondo.png"];
            id_fondo=1;
            break;
    }
    //id_fondo++;
    [self setNeedsDisplay:YES];
}

-(void)handleIniciarAnimacion:(NSNotification *)aNotification
{
    if(temporizador == nil)
    {
        NSLog(@"Comienza la animación\n");
        temporizador = [NSTimer scheduledTimerWithTimeInterval:0.04
                                                        target:self
                                                      selector:@selector(siguienteMovimiento:)
                                                      userInfo:nil
                                                       repeats:YES];
    }
    else
    {
        NSLog(@"Parando animación\n");
        NSBeep();
        [temporizador invalidate];
        temporizador=nil;
        //rectanguloAnimado.origin.x=25;
        //rectanguloAnimado.origin.y=25;
    }
    
    if([[[aNotification userInfo] objectForKey:@"detener"] integerValue] == 1)
    {
        NSLog(@"Deteniendo animación\n");
        NSBeep();
        [temporizador invalidate];
        temporizador=nil;
        rectanguloAnimado.origin.x=0;
        rectanguloAnimado.origin.y=0;
        impacto=NO;
        imagenAnimado = [NSImage imageNamed:@"angry.png"];
        [self setNeedsDisplay:YES];
    }
}

-(void)siguienteMovimiento:(NSTimer *)aTimer
{
    
    NSRect bounds = [self bounds];
    NSPoint puntoActual;
    NSPoint siguientePunto;
    int i;
    
    // Proceso de animación al siguiente punto de la gráfica
        
    if(myPath==nil)
    {
        NSLog(@"ERROR: No hay gráfica.\n");
        [temporizador invalidate];
        temporizador=nil;
    }
    
    NSLog(@"Entrando el Temporizador. Siguiente punto de animación\n");
    puntoActual.x = rectanguloAnimado.origin.x;
    puntoActual.y = rectanguloAnimado.origin.y;

    siguientePunto.x = puntoActual.x + 5; // cambiar velocidad de la animación
    i = siguientePunto.x;
    
    siguientePunto.y = [experimento valueAt:i bounds:bounds];
    
    rectanguloAnimado.origin.x=siguientePunto.x;
    rectanguloAnimado.origin.y=siguientePunto.y;
    
    // Rotación
    //DESARROLLOOOOOOO
        tf = [NSAffineTransform transform];
        [tf rotateByDegrees:25];
    [imagenAnimado lockFocus];
    [tf concat];
    [imagenAnimado unlockFocus];
    [imagenAnimado drawInRect:rectanguloAnimado];
    
    
    // Control de toque de Tierra
    if((siguientePunto.y < 25) && (puntoActual.y > siguientePunto.y)) /* Antes 0 para toque tierra. 25 para impacto
                                                                     de parte inferior de la imagen*/
    {
        // Tierra
        imagenAnimado = [NSImage imageNamed:@"img_explosion.png"];
    }
    else if (imagenAnimado != [NSImage imageNamed:@"angry.png"]) /* añado comprobacion, para no estar repintando
                                                                  la figura continuamente cuando está en Aire*/
    {
        if(!impacto)
            imagenAnimado = [NSImage imageNamed:@"angry.png"];
    }
    if (siguientePunto.y <= -25)
    {
        // Reiniciar animacion
        rectanguloAnimado.origin.x=puntoReferencia.x-25;
        rectanguloAnimado.origin.y=puntoReferencia.y-25;
        imagenAnimado = [NSImage imageNamed:@"angry.png"]; // Imagen para cargar en el rectangulo animado
        
        impacto=NO; // reiniciar bandera para el siguiente disparo
    }
    
    // Control de impacto
    if(!impacto)
    {
            // Control de Altura, se considera impacto al 75% de altura del objetivo
        if ((rectanguloAnimado.origin.y < (rectanguloObjetivo.origin.y+(rectanguloObjetivo.size.height*0.75))) && (rectanguloAnimado.origin.y > rectanguloObjetivo.origin.y))
        {
       
            // Control choque por derecha
            if((rectanguloAnimado.origin.x > rectanguloObjetivo.origin.x) && (rectanguloAnimado.origin.x < (rectanguloObjetivo.origin.x+rectanguloObjetivo.size.width)))
            {
                [self realizarImpacto];
            }
        
            // Control choque por izquierda
            if(((rectanguloAnimado.origin.x+rectanguloAnimado.size.width) > rectanguloObjetivo.origin.x) && ((rectanguloAnimado.origin.x+rectanguloAnimado.size.width) < (rectanguloObjetivo.origin.x + rectanguloObjetivo.size.width)))
            {
                [self realizarImpacto];
            }
        
        }
    }
    
    [self setNeedsDisplay:YES];
    
    // Sonidos
    if(puntoActual.x==puntoReferencia.x)
        [soundDisparo play];
}

-(void)calcularNuevaPosicion:(NSTimer *)aTimer
{
    srandom((int)time(NULL)); // generando semilla
    
    NSLog(@"Entrando al temporizador - generar nueva posicion X");
    
    NSRect bounds = [self bounds];
    
    //double coorX;
    
    coorX = (int)random() % (int)(bounds.size.width - rectanguloObjetivo.size.width - puntoReferencia.x) + puntoReferencia.x;
    /* calculo posicion X aleatoriamente */
    
    //rectanguloObjetivo.origin.x=coorX;
    
    [temporizadorNuevaPosicion invalidate];
    temporizadorDesplazamiento = [NSTimer scheduledTimerWithTimeInterval:0.04
                                                                  target:self
                                                                selector:@selector(moverNuevaPosicion:)
                                                                userInfo:nil
                                                                 repeats:YES];
    
    //depuracion
    rectDestino.size.width=80;
    rectDestino.size.height=80;
    rectDestino.origin.x=coorX;
    [self setNeedsDisplay:YES];
}

-(void)moverNuevaPosicion:(NSTimer *)aTimer
{
    if (coorX < rectanguloObjetivo.origin.x) // desplazamiento a izquierda
    {
        imagenObjetivo = [NSImage imageNamed:@"green-pig.png"];
        rectanguloObjetivo.origin.x-=3;
        if (coorX > rectanguloObjetivo.origin.x) // Si nos pasamos al restar 2. Sumar 1 para compensar numeros impares.
        {
            //[temporizadorDesplazamiento invalidate];
            rectanguloObjetivo.origin.x+=2;
        }
    }
    else // desplazamiento a derecha
    {
        imagenObjetivo = [NSImage imageNamed:@"green-pig-volteado.png"];
        rectanguloObjetivo.origin.x+=3;
        if (coorX < rectanguloObjetivo.origin.x)
        {
            //[temporizadorDesplazamiento invalidate];
            rectanguloObjetivo.origin.x-=2;
        }
    }
    
    if (coorX == rectanguloObjetivo.origin.x)
    {
        [temporizadorDesplazamiento invalidate];
        
        temporizadorNuevaPosicion = [NSTimer scheduledTimerWithTimeInterval:3
                                                                         target:self
                                                                       selector:@selector(calcularNuevaPosicion:)
                                                                       userInfo:nil
                                                                        repeats:YES];
    }
    [self setNeedsDisplay:YES];
    //NSLog(@"LLEGUE A DESTINO\n");
}

-(void)botarObjetivo:(NSTimer *)aTimer
{
    // Botar
    switch (salto)
    {
        case 1:
            rectanguloObjetivo.origin.y+=2.5;
            salto++;
            break;
        case 2:
            rectanguloObjetivo.origin.y+=2.5;
            salto++;
            break;
        case 3:
            rectanguloObjetivo.origin.y-=2.5;
            salto++;
            break;
        case 4:
            rectanguloObjetivo.origin.y-=2.5;
            salto++;
            break;
        default:
            salto=1;
            break;
    }
    [self setNeedsDisplay:YES];
}

-(void)realizarImpacto
{
    impacto=YES;

    NSString * notificationName = @"aumentarContadorNotification";
    NSLog(@"Enviando notificacion de AUMENTAR contador");
    NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:notificationName object:self];
    
    [soundImpacto play];
        
    [temporizadorDesplazamiento invalidate];
    [temporizadorNuevaPosicion invalidate];
    imagenObjetivo = [NSImage imageNamed:@"img_explosion.png"];
    imagenAnimado = [NSImage imageNamed:@"img_explosion.png"];
    temporizadorNuevaPosicion = [NSTimer scheduledTimerWithTimeInterval:3
                                                                 target:self
                                                               selector:@selector(calcularNuevaPosicion:)
                                                               userInfo:nil
                                                                repeats:YES];
}

-(void)handleSonidoOnOff:(NSNotification *)aNotification
{
    NSLog(@"EVENTOR ECBIDO");
    if(soundImpacto!=nil)
    {
        soundDisparo = nil;
        soundImpacto = nil;
    }
    else
    {
        soundDisparo = [NSSound soundNamed:@"angry_birds_sonido_lanzamiento.wav"];
        soundImpacto = [NSSound soundNamed:@"angry_birds_lanzamiento_del_big_brother.wav"];
    }
}

@end


