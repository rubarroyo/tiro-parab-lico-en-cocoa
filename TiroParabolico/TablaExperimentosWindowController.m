//
//  TablaExperimentosWindowController.m
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 14/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import "TablaExperimentosWindowController.h"
#import "TiroParabolico.h"

@interface TablaExperimentosWindowController ()

@end

@implementation TablaExperimentosWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    NSLog(@"Fichero NIB cargado");
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

-(void)awakeFromNib
{
    [angleTextField setDoubleValue:45];
    
    [buttonDelete setEnabled:NO];
    [loadExperiment setEnabled:NO];
    [buttonSaveExperiment setEnabled:NO];
}

-(id) init
{
    if (![super initWithWindowNibName:@"TablaExperimentosWindow"])
        return nil;
    tParabolico=[[TiroParabolico alloc] init];
    experimentosArray=[[NSMutableArray alloc] init];
    return self;
}

-(id)initWithWindow:(NSWindow *)window
{
    self=[super initWithWindow:window];
    if(self){
        // initialization code here.
    }
    return self;
}

-(void)controlTextDidChange:(NSNotification *)notification
{
    // Sender del evento:
    NSString * sender = [[notification object] identifier];
    
    NSLog(@"Notificacion de ediccion en TextField: %@\n", sender);
    
    // TextField Velocidad
    if(sender == [speedTextField identifier])
    {
        NSLog(@"Evento enviado desde el TextField de velocidad\n");
        tParabolico.V0=[speedTextField doubleValue];
        NSLog(@"Velocidad cambiada %f",tParabolico.V0);
        [speedTextField setDoubleValue:tParabolico.V0]; // la propiedad V0 de tParabolico me asegura que no se
                                                        // pasa ninguna dato de tipo carácter
        [self actualizarTextFields];
    }
    
    // TextField Angulo
     else if(sender == [angleTextField identifier])
    {
        NSLog(@"Evento enviado desde el TextFielde de angulo\n");
        [angleSlider setDoubleValue:[angleTextField doubleValue]];
        [angleTextField setDoubleValue:[angleSlider doubleValue]]; // Slider asegura un valor entre 0-90.
        tParabolico.alfa=[angleTextField doubleValue];
        NSLog(@"Angulo cambiado\n");
        [self actualizarTextFields];
    }
}

-(IBAction)angleSlider:(id)sender
{
    [angleTextField setDoubleValue:[sender doubleValue]];
    tParabolico.alfa=[angleTextField doubleValue];
    NSLog(@"Angulo cambiado\n");
    [self actualizarTextFields];
}

-(void)actualizarTextFields
{
    if(([tParabolico V0]>=0) && [tParabolico alfa]>=0 && [tParabolico alfa]<=90)
    {
        [tParabolico CalcularDatosFisicos];
        [totalDistanceTextField setDoubleValue:[tParabolico distancia_max]];
        [totalTimeTextField setDoubleValue:[tParabolico tiempo_total]];
        [maximumHeightTextField setDoubleValue:[tParabolico altura_max]];
        
        [buttonSaveExperiment setEnabled:YES];
        
        // Enviar notificación
        [self enviarEvento:tParabolico];
    }
    else
        NSLog(@"ERROR AL ACTUALIZAR TEXTFIELDS Y DATOS FISICOS.\n");
    
}

-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [experimentosArray count];
}

-(id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    TiroParabolico * experimento = [experimentosArray objectAtIndex:row];
    
    NSLog(@"Identificador de columna: %@\n",[tableColumn identifier]);
    
    if ([[tableColumn identifier] isEqual:@"initialSpeed"])
    {
        return [NSNumber numberWithDouble:experimento.V0];
    }
    else if ([[tableColumn identifier] isEqual:@"angle"])
    {
        return [NSNumber numberWithDouble:experimento.alfa];
    }
    else if ([[tableColumn identifier] isEqual:@"distance"])
    {
        return [NSNumber numberWithDouble:experimento.distancia_max];
    }
    else if ([[tableColumn identifier] isEqual:@"totaltime"])
    {
        return [NSNumber numberWithDouble:experimento.tiempo_total];
    }
    else if ([[tableColumn identifier] isEqual:@"maxheight"])
    {
        return [NSNumber numberWithDouble:experimento.altura_max];
    }
    else
    {
        NSLog(@"ERROR: Identificadores de columna no válidos.\n");
        return @"ERROR";
    }
    
}

-(void)tableViewSelectionDidChange:(NSNotification *)notification
{
    NSInteger row = [aTableView selectedRow];
    if(row!=-1)
    {
        [buttonDelete setEnabled:YES];
        [loadExperiment setEnabled:YES];
    }
    else
    {
        [buttonDelete setEnabled:NO];
        [loadExperiment setEnabled:NO];
    }
}

-(IBAction)deleteExperiment:(id)sender
{
    NSInteger row = [aTableView selectedRow];
    if(row!=-1)
    {
        NSInteger respuesta;
        respuesta=NSRunAlertPanel(@"Eliminar Experimento", @"¿Desea eliminar el experimento?", @"No", @"Sí", nil);
        
        if(respuesta==NSAlertAlternateReturn){
            [experimentosArray removeObjectAtIndex:row];
            [buttonDelete setEnabled:NO];
            [loadExperiment setEnabled:NO];
            [self actualizarTextFields];
            [aTableView reloadData];
            NSRunAlertPanel(@"Experimento eliminado", @"El experimento ha sido eliminado correctamente", @"Aceptar", nil, nil);
        }
    }
    else
        return;
}

-(IBAction)loadExperiment:(id)sender
{
    TiroParabolico * experimentoSeleccionado;
    
    NSInteger row = [aTableView selectedRow];
    if(row!=-1)
    {
        // Cargar experimento para el Visor gráfico
        experimentoSeleccionado = [experimentosArray objectAtIndex:row];
        [self enviarEvento:experimentoSeleccionado];
        
        //NSRunAlertPanel(@"Experimento cargado", @"Puedes visualizar el experimento en el ""Visor Gráfico""", @"Aceptar", nil, nil);
    }
    else
        return;
}

-(IBAction)saveExperiment:(id)sender
{
    [experimentosArray addObject:tParabolico];
    [buttonSaveExperiment setEnabled:NO];
    [aTableView reloadData];
    
    tParabolico=[[TiroParabolico alloc] initWithValues:[speedTextField doubleValue] alfa:[angleTextField doubleValue]];
}

-(void)enviarEvento:(TiroParabolico *)experimento
{
    NSNotificationCenter * nc;
    nc=[NSNotificationCenter defaultCenter];
    
    NSString * notificationName = @"LoadExperimentToShow";
    
    NSDictionary * datosExperimento;
    NSArray * valores;
    NSArray * keys = [[NSArray alloc] initWithObjects:@"velocidad", @"angulo", nil];
    
    NSNumber *velocidad = [NSNumber numberWithDouble:experimento.V0];
    NSNumber *angulo = [NSNumber numberWithDouble:experimento.alfa];
    
    valores = [[NSArray alloc] initWithObjects:velocidad, angulo, nil];
    
    datosExperimento = [[NSDictionary alloc] initWithObjects:valores
                                                     forKeys:keys];
    
    [nc postNotificationName:notificationName object:self userInfo:datosExperimento];
}

@end


