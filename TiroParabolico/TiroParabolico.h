//
//  TiroParabolico.h
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 18/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TiroParabolico : NSObject
{
    // Declaracion de variables de instancia para el experimento.
        
    // Variables Físicas
    double g;
    double V0;
    double alfa;
    double t;
    double distancia_max;
    double altura_max;
    double tiempo_total;
    NSString * nombre;
}

// Propiedades
@property (nonatomic) double g;
@property (nonatomic) double V0;
@property (nonatomic) double alfa;
@property (nonatomic) double t;
@property (nonatomic) double distancia_max;
@property (nonatomic) double altura_max;
@property (nonatomic) double tiempo_total;
      
// Declaración de los Métodos

-(id) init;

-(id) initWithValues:(double)velocidad alfa:(double)angulo;

-(void) CalcularDatosFisicos;
        
-(double) calcular_xrealmax;

-(NSMutableArray *) CalcularPuntosReales: (int) numpuntos
                            xrealmax: (double) xrealmax
                            xrealmin: (double) xrealmin;

-(double) valueAt: (float)x
           bounds: (NSRect)bounds;

@end
