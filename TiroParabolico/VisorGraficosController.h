//
//  VisorGraficosController.h
//  TiroParabolico
//
//  Created by Ruben Arroyo Rodriguez on 19/12/15.
//  Copyright © 2015 Ruben Arroyo Rodriguez. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VisorGraficosController : NSWindowController
{
    IBOutlet NSButton * buttonFondo;
    IBOutlet NSColorWell * selectorColor;
    IBOutlet NSSlider * sliderOpacidad;
    IBOutlet NSButton * buttonIniciarAnimacion;
    IBOutlet NSTextField * contadorImpactos;
    
    IBOutlet NSButton * checkboxSonido;
    
    NSNotificationCenter * nc;
}

-(IBAction)colorSeleccionado:(id)sender;
-(IBAction)fondoImagenButton:(id)sender;

-(IBAction)iniciarAnimacion:(id)sender;
-(IBAction)detenerAnimacion:(id)sender;

-(IBAction)sonidoCheckBox:(id)sender;

-(void)handleAumentarContador:(NSNotification *)aNotification;

@end
